@extends('layouts.admin')

@section('page_header', 'Resolution Center')

@section('styles')

<link rel="stylesheet" href="{{asset('admin-assets/css/dropify.min.css')}}">

<link href="{{asset('admin-assets/css/datepicker.css')}}" rel="stylesheet">

@endsection

@section('breadcrumbs')


<li class="breadcrumb-item active"><a href="javascript:void(0)" aria-current="page"></a><span>{{tr('resolution_center')}}</span></li>

@endsection

@section('content')

<div class="">
    <div class="row no-gutters">

        @if($user_disputes->count())

        <div class="col-lg-5 col-xxl-4">
            <div class="card card-default chat-left-sidebar">

                <form class="card-header px-0">
                    <div class="input-group px-5">
                        <input type="text" id="search" class="form-control" aria-label="{{tr('user_name_search_placeholder')}}" placeholder="{{tr('user_name_search_placeholder')}}" />
                    </div>
                </form>


                <ul class="card-body px-0 search-disputes" data-simplebar style="height: 630px;">
                    @foreach($user_disputes as $key=>$dispute_details)

                    <li class="mb-4 px-5 py-2">
                        @if($key==0)
                        <a href="javascript:void(0)" class="media media-message dispute_active_user user_dispute_view" attr="{{$dispute_details->id}}">
                            @else
                            <a href="javascript:void(0)" class="media media-message user_dispute_view" attr="{{$dispute_details->id}}">
                                @endif
                                <div class="position-relative mr-3">
                                    <img class="rounded-circle" style="width:40px" src="{{$dispute_details->sender->picture??''}}" alt="User Image" />
                                    <span class="status away"></span>
                                </div>

                                <div class="media-body">
                                    <div class="message-contents">
                                        <span class="d-flex justify-content-between align-items-center mb-1">
                                            <span class="username">{{$dispute_details->sender->name ?? ''}}</span>
                                            <span class="">
                                                <span class="state text-smoke"><em>{{$dispute_details->created_at->diffForHumans()?? ''}}</em></span>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </a>
                    </li>

                    @endforeach

                </ul>

            </div>

        </div>


        <div class="col-lg-7 col-xxl-8">
            <!-- Chat -->



            <div class="card card-default chat-right-sidebar" id="inbox">
                @include('admin.resolution-center._inboxes')
            </div>


        </div>
        @else
        <h4 class="pull-right msg_css">{{tr('no_messages_found')}}</h4>


        @endif
    </div>

</div>



@endsection

@section('scripts')

<script type="text/javascript">
    // ***** Inline search of dispute user

    var $rows = $('.search-disputes li');
    $('#search').keyup(function() {
        var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

        $rows.show().filter(function() {
            var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
            return !~text.indexOf(val);
        }).hide();
    });






    $('body').on('click', '.user_dispute_view', function() {

        $('.user_dispute_view').removeClass('dispute_active_user');

        $(this).addClass('dispute_active_user');


        $.ajaxSetup({
            headers: {
                'X-CSSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var user_dispute_id = $(this).attr('attr');

        $.post("{{route('admin.inboxes.view')}}", {
            user_dispute_id: user_dispute_id,
            _token: "{{ csrf_token() }}",
        }).done(function(resp) {
            if (resp.error) {

            } else {

                $('#inbox').html(resp.data);
            }
        });


    });
</script>
@endsection
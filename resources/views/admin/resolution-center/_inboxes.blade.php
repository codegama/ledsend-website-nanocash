

@if($inbox)

<div class="card-header">

@foreach($inbox as $key => $inboxes)

    @if($key==0)
    <h2>{{$inbox->sender->name}}</h2>
    <div class="dropdown">
        <div class="dropdown">
            <a class="dropdown-toggle icon-burger-mini" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                <a class="dropdown-item" href="user-profile-settings.html">Profile</a>
                <a class="dropdown-item" href="javascript:void(0)">Logout</a>
            </div>
        </div>
    </div>
    @endif
@endforeach

</div>

<div class="card-body pb-0" data-simplebar style="height: 545px;">
    <!-- Media Chat Left -->
    @foreach($inbox as $key => $inboxes)

    @if($inboxes->message_type == USER_TO_USER)
    <div class="media media-chat">
        <img src="{{$inbox->sender->picture??''}}" class="rounded-circle" alt="Avata Image" style="width:50px"/>
        <div class="media-body">
            <div class="text-content">
                <span class="message">{{$inboxes->message}}.</span>
                <time class="time">{{$inboxes->created_at->diffForHumans()?? ''}}</time>
            </div>
        </div>
    </div>
    @endif

    @if($inboxes->message_type == ADMIN_TO_USER)
    <!-- Media Chat Right -->
    <div class="media media-chat media-chat-right">
        <div class="media-body">
            <div class="text-content">
                <span class="message">{{$inboxes->message}}.</span>
                <time class="time">{{$inboxes->created_at->diffForHumans()?? ''}}</time>
            </div>

        </div>
        <img src="{{$inbox->sender->picture??''}}" class="rounded-circle" alt="Avata Image" style="width:50px"/>
    </div>
    @endif

    @endforeach

</div>

<div class="chat-footer">
    <form>
        <div class="input-group input-group-chat">
            <div class="input-group-prepend">
                <span class="emoticon-icon mdi mdi-emoticon-happy-outline"></span>
            </div>
            <input type="text" class="form-control" aria-label="Text input with dropdown button" />
        </div>
    </form>
</div>
@endif
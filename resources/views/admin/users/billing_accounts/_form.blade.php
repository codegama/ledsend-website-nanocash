<div class="card-body">

    @if(Setting::get('is_demo_control_enabled') == NO )

    <form method="POST" action="{{route('admin.user_billing_accounts.save')}}" role="form">

        @else

        <form class="forms-sample" role="form">

            @endif

            @csrf

            <div class="form-body">


                <input type="hidden" name="user_id" value="{{Request::get('user_id')?? $user_details->user_id}}">


                <input type="hidden" name="billing_account_id" value="{{Request::get('billing_account_id')??''}}">

                <div class="row p-t-20">

                    <div class="form-group col-md-6">

                        <label class="control-label">{{tr('nickname')}}<span class="admin-required">*</span></label>

                        <input type="text" name="nickname" value="{{old('nickname') ?: $user_details->nickname}}" class="form-control" placeholder="{{tr('nickname')}}" required>

                    </div>

                    <div class="form-group col-md-6">

                        <label class="control-label">{{tr('account_holder_name')}}<span class="admin-required">*</span></label>
                        <input type="text" name="account_holder_name" value="{{old('account_holder_name') ?: $user_details->account_holder_name}}" class="form-control" placeholder="{{tr('account_holder_name')}}" required>

                    </div>


                    <div class="form-group col-md-6">

                        <label class="control-label">{{tr('account_no')}}<span class="admin-required">*</span></label>
                        <input type="number" min="1" step="any" class="form-control" name="account_number" value="{{old('account_number') ?:  $user_details->account_number}}" placeholder="867676576" required>

                    </div>

                    <div class="form-group col-md-6">

                        <label class="control-label">
                            {{tr('bank_name')}}<span class="admin-required">*</span>
                        </label>

                        <input type="text" class="form-control" name="bank_name" value="{{old('bank_name') ?:  $user_details->bank_name}}" placeholder="ABCD Bank" required>

                    </div>

                    <div class="form-group col-md-6">

                        <label class="control-label">{{tr('ifsc_code')}}<span class="admin-required">*</span></label>
                        <input type="text" class="form-control" name="ifsc_code" value="{{old('ifsc_code') ?: $user_details->ifsc_code}}" placeholder="{{tr('ifsc_code')}}" required>

                    </div>

                    <div class="form-group col-md-6">

                        <label class="control-label">{{tr('swift_code')}}<span class="admin-required">*</span></label>
                        <input type="text" class="form-control" name="swift_code" value="{{old('swift_code') ?: $user_details->swift_code}}" placeholder="{{tr('swift_code')}}" required>

                    </div>



                </div>



            </div>

            <div class="form-actions">

                @if(Setting::get('is_demo_control_enabled') == NO )

                <button type="submit" class="btn btn-primary btn-pill pull-right"> <i class="fa fa-check"></i>{{tr('save')}}</button>

                @else

                <button type="button" class="btn btn-primary btn-pill pull-right"> <i class="fa fa-check"></i>{{tr('save')}}</button>

                @endif

                <a  class="btn reset-btn btn-pill" href="{{route('admin.user_billing_accounts.index',['user_id'=> Request::get('user_id')?? $user_details->user_id])}}">{{tr('reset')}}</a>

            </div>

        </form>

</div>
@extends('layouts.admin')

@section('page_header',tr('user_wallets'))

@section('breadcrumbs')


<li class="breadcrumb-item active"><a href="javascript:void(0)"></a>{{tr('user_wallets')}}</li>

@endsection

@section('content')

<div class="card">

    <div class="card-header bg-info">

        <h4 class="m-b-0 text-white">{{tr('user_wallets')}}

            <button type="button" class="badge badge-square badge-outline-light" data-toggle="popover" data-content="{{tr('user_wallets_note')}}">?</button>

        </h4>

    </div>

    <div class="card-body">

        <div class="row">

            <div class="col-6">
                <div class="card card-default">
                    <div class="card-body">
                        <table class="table table-borderless table-thead-border">
                            <tbody>
                                <tr>
                                    <td class="text-dark font-weight-bold">{{tr('username')}}</td>
                                    <td class="text-right text-uppercase">
                                        <a href="{{route('admin.users.view',['user_id' => $wallet_payment_details->user_id])}}">
                                            {{ $wallet_payment_details->user->name ?? "" }}
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-dark font-weight-bold">{{tr('email')}}</td>
                                    <td class="text-right">{{$wallet_payment_details->user->email ?? "-"}}</td>
                                </tr>
                                <tr>
                                    <td class="text-dark font-weight-bold">{{tr('mobile')}}</td>
                                    <td class="text-right">{{$wallet_payment_details->user->mobile ?: tr('not_available')}}</td>
                                </tr>
                            </tbody>
                        </table>
                    
                    </div>
                    <hr>
                    <div class="card-header">
                        <h2>{{tr('billing_accounts')}}</h2>
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless table-thead-border">
                            <thead>
                                <tr>
                                    <th>Label</th>
                                    <th class="text-right">Details</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-dark font-weight-bold">{{tr('account_holder_name')}}</td>
                                    <td class="text-right">{{$wallet_payment_details->billingaccountDetails->account_holder_name ?? tr('not_available')}}</td>
                                </tr>
                                
                                <tr>
                                    <td class="text-dark font-weight-bold">{{tr('account_no')}}</td>
                                    <td class="text-right">{{$wallet_payment_details->billingaccountDetails->account_number ?? tr('not_available')}}</td>
                                </tr>

                                <tr>
                                    <td class="text-dark font-weight-bold">{{tr('bank_name')}}</td>
                                    <td class="text-right">{{$wallet_payment_details->billingaccountDetails->bank_name ?? tr('not_available')}}</td>
                                </tr>
                               
                                <tr>
                                    <td class="text-dark font-weight-bold">{{tr('ifsc_code')}}</td>
                                    <td class="text-right">{{$wallet_payment_details->billingaccountDetails->ifsc_code ?? tr('not_available')}}</td>
                                </tr>
                                <tr>
                                    <td class="text-dark font-weight-bold">{{tr('swift_code')}}</td>
                                    <td class="text-right">{{$wallet_payment_details->billingaccountDetails->swift_code ?? tr('not_available')}}</td>
                                </tr>

                               
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

            <div class="col-6">

                <div class="card card-default">

                    <div class="card-body">
                        <div class="card card-default">
                            <div class="d-flex p-5 justify-content-between">
                                <div class="icon-md bg-warning rounded-circle mr-3">
                                    <i class="mdi mdi-wallet"></i>
                                </div>
                                <div class="text-right">
                                    <span class="h2 d-block">{{$wallet_payment_details->paid_amount_formatted}}</span>
                                    <p>{{tr('paid_amount')}}</p>
                                </div>
                            </div>

                        </div>

                      

                        <table class="table table-borderless table-thead-border">
                            <tbody>
                                <tr>
                                    <td class="text-dark font-weight-bold">{{tr('status')}}</td>
                                    <td class="text-right text-uppercase">{{$wallet_payment_details->status_formatted}}</td>
                                </tr>

                                <tr>
                                    <td class="text-dark font-weight-bold">{{tr('paid_amount')}}</td>
                                    <td class="text-right text-uppercase">{{$wallet_payment_details->paid_amount_formatted}}</td>
                                </tr>

                                <tr>
                                    <td class="text-dark font-weight-bold">{{tr('paid_date')}}</td>
                                    <td class="text-right text-uppercase">{{ common_date($wallet_payment_details->paid_date , Auth::guard('admin')->user()->timezone) }}</td>
                                </tr>

                                <tr>
                                    <td class="text-dark font-weight-bold">{{tr('payment_mode')}}</td>
                                    <td class="text-right text-uppercase">{{ $wallet_payment_details->payment_mode  }}</td>
                                </tr>

                            </tbody>
                        </table>
                    
                    </div>
                
                </div>
                
            </div>

    </div>

</div>

@endsection
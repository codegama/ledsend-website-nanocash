@extends('layouts.admin')

@section('page_header',tr('user_wallets'))

@section('breadcrumbs')


<li class="breadcrumb-item active"><a href="javascript:void(0)"></a>{{tr('withdraw_requests')}}</li>

@endsection

@section('content')

<div class="card">

    <div class="card-header bg-info">

        <h4 class="m-b-0 text-white">{{tr('withdraw_requests')}}

            <button type="button" class="badge badge-square badge-outline-light" data-toggle="popover" data-content="{{tr('user_withdraw_note')}}">?</button>

        </h4>

    </div>

    <div class="card-body">

        @include('admin.withdrawals._search')

        <div class="table-responsive">

            <table id="dataTable" class="table data-table">

                <thead>
                    <tr>
                        <th>{{ tr('s_no') }}</th>
                        <th>{{ tr('name') }}</th>
                        <th>{{ tr('email') }}</th>
                        <th>{{ tr('requested') }}</th>
                        <th>{{ tr('paid') }}</th>
                        <th>{{ tr('wallets') }}</th>
                        <th>{{tr('status')}}</th>
                        <th>{{ tr('action') }}</th>
                    </tr>
                </thead>

                <tbody>

                    @foreach($user_withdrawals as $i => $withdrawal_details)

                    <tr>

                        <td>{{$i+$user_withdrawals->firstItem()}}</td>


                        <td>
                            <a href="{{route('admin.users.view' , ['user_id' => $withdrawal_details->user_id])}}"> {{ $withdrawal_details->user->name ?? tr('user_not_avail')}}
                            </a>
                        </td>

                        <td>{{ $withdrawal_details->user->email ?: tr('not_available')}}
                            <span>
                                <h6>{{ $withdrawal_details->user->mobile ?? tr('not_available')}}
                                </h6>
                            </span>

                        </td>


                        <td class="text-success">{{$withdrawal_details->requested_amount_formatted}}</td>

                        <td>{{$withdrawal_details->paid_amount_formatted}}</td>


                        <td>
                            <a class="btn btn-outline-warning btn-sm" href="{{ route('admin.user_wallets.view', ['user_id' => $withdrawal_details->id]) }}">
                                {{tr('wallets')}}
                            </a>
                        </td>

                        <td>
                            @if($withdrawal_details->status == WITHDRAW_INITIATED)

                            <span class="text-info">{{tr('requested')}}</span>

                            @elseif($withdrawal_details->status == WITHDRAW_PAID)

                            <span class="text-success">{{tr('paid')}}</span>

                            @elseif($withdrawal_details->status == WITHDRAW_ONHOLD)

                            <span class="text-warning">{{tr('on_hold')}}</span>
                            @else
                            
                            <span class="text-danger">{{tr('declined')}}</span>
                            @endif
                        </td>

                        <td class="flex">

                            @include('admin.withdrawals._actions')

                        </td>

                    </tr>

                    @endforeach

                </tbody>

            </table>

            <div class="pull-right">{{ $user_withdrawals->appends(request()->input())->links() }}</div>


        </div>

    </div>

</div>


@foreach($user_withdrawals as $i => $withdrawal_details)

<div id="paynowModal{{$i}}" class="modal fade" role="dialog">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <h4 class="modal-title pull-left">
                    <a href="{{route('admin.users.view' , ['user_id' => $withdrawal_details->user_id])}}"> {{ $withdrawal_details->user->name ?? tr('user_not_avail')}}
                    </a>
                </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

                <div class="row">

                    <div class="col-sm popup-label">
                        <b class="label-font">{{tr('account_holder_name')}}</b>
                        <p>{{$withdrawal_details->billingaccountDetails->account_holder_name ?? tr('not_available') }}</p>
                    </div>

                    <div class="col-sm popup-label">
                        <b class="label-font">{{tr('account_number')}}</b>
                        <p>{{$withdrawal_details->billingaccountDetails->account_number ?? tr('not_available') }}</p>
                    </div>

                </div>

                <div class="row">

                    <div class="col-sm popup-label">
                        <b class="label-font">{{tr('bank_name')}}</b>
                        <p>{{$withdrawal_details->billingaccountDetails->bank_name ?? tr('not_available') }}</p>
                    </div>

                    <div class="col-sm popup-label">
                        <b class="label-font">{{tr('ifsc_code')}}</b>
                        <p>{{$withdrawal_details->billingaccountDetails->ifsc_code ?? tr('not_available') }}</p>
                    </div>

                    
                </div>

                <div class="row">

                    <div class="col-sm popup-label">
                        <b class="label-font">{{tr('swift_code')}}</b>
                        <p>{{$withdrawal_details->billingaccountDetails->swift_code ?? tr('not_available') }}</p>
                    </div>

                    <div class="col-sm popup-label">
                        <b class="label-font">{{tr('created_at')}}</b>
                        <p>{{ common_date($withdrawal_details->created_at,Auth::guard('admin')->user()->timezone,'d M Y') }}</p>
                    </div>

                </div>

            </div>


            <div class="row">

                     <div class="col-sm popup-label">
                        <b class="label-font">{{tr('requested_amount')}}</b>
                        <p>{{formatted_amount($withdrawal_details->requested_amount ?? '0.00')}}</p>
                    </div>


            </div>

            <div class="modal-footer">

                <form class="forms-sample" action="{{ route('admin.user_withdrawals.paynow', ['user_withdrawal_id' => $withdrawal_details->id]) }}" method="GET" role="form">
                    @csrf

                    <input type="hidden" name="user_withdrawal_id" id="user_withdrawal_id" value="{{$withdrawal_details->id}}">

                    <button type="submit" class="btn btn-info btn-sm" onclick="return confirm(&quot;{{tr('user_withdrawal_paynow_confirmation')}}&quot;);">{{tr('paynow')}}</button>

                    <button type="button" class="btn btn-outline-primary btn-sm" data-dismiss="modal">{{tr('close')}}</button>
                </form>

                
            </div>
        </div>

    </div>
</div>
@endforeach

@endsection
@extends('layouts.admin')

@section('page_header',tr('withdraw_requests'))

@section('breadcrumbs')


<li class="breadcrumb-item active"><a href="javascript:void(0)"></a>{{tr('withdraw_requests')}}</li>

@endsection

@section('content')

<div class="card">

    <div class="card-header bg-info">

        <h4 class="m-b-0 text-white">{{tr('withdraw_requests')}}

            <button type="button" class="badge badge-square badge-outline-light" data-toggle="popover" data-content="{{tr('user_withdraw_note')}}">?</button>

        </h4>

    </div>

    <div class="card-body">

        <div class="row">

            <div class="col-6">
                <div class="card card-default">
                    <div class="card-body">
                        <table class="table table-borderless table-thead-border">
                            <tbody>
                                <tr>
                                    <td class="text-dark font-weight-bold">{{tr('username')}}</td>
                                    <td class="text-right text-uppercase">
                                        <a href="{{route('admin.users.view',['user_id' => $user_withdrawal_details->user_id])}}">
                                            {{ $user_withdrawal_details->user->name ?? "" }}
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-dark font-weight-bold">{{tr('email')}}</td>
                                    <td class="text-right">{{$user_withdrawal_details->user->email ?? "-"}}</td>
                                </tr>
                                <tr>
                                    <td class="text-dark font-weight-bold">{{tr('mobile')}}</td>
                                    <td class="text-right">{{$user_withdrawal_details->user->mobile ?? "-"}}</td>
                                </tr>
                            </tbody>
                        </table>
                    
                    </div>
                    <hr>
                    <div class="card-header">
                        <h2>{{tr('billing_accounts')}}</h2>
                    </div>
                    <div class="card-body">
                        <table class="table table-borderless table-thead-border">
                            <thead>
                                <tr>
                                    <th>Label</th>
                                    <th class="text-right">Details</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-dark font-weight-bold">{{tr('account_holder_name')}}</td>
                                    <td class="text-right">{{$billing_account_details->account_holder_name ?? "-"}}</td>
                                </tr>
                                <tr>
                                    <td class="text-dark font-weight-bold">{{tr('account_no')}}</td>
                                    <td class="text-right">{{$billing_account_details->account_number ?? "-"}}</td>
                                </tr>
                                <tr>
                                    <td class="text-dark font-weight-bold">{{tr('ifsc_code')}}</td>
                                    <td class="text-right">{{$billing_account_details->ifsc_code ?? "-"}}</td>
                                </tr>
                                <tr>
                                    <td class="text-dark font-weight-bold">{{tr('swift_code')}}</td>
                                    <td class="text-right">{{$billing_account_details->swift_code ?? "-"}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

            <div class="col-6">

                <div class="card card-default">

                    <div class="card-body">
                        <div class="card card-default">
                            <div class="d-flex p-5 justify-content-between">
                                <div class="icon-md bg-warning rounded-circle mr-3">
                                    <i class="mdi mdi-wallet"></i>
                                </div>
                                <div class="text-right">
                                    <span class="h2 d-block">{{$user_withdrawal_details->requested_amount_formatted}}</span>
                                    <p>{{tr('requested_amount')}}</p>
                                </div>
                            </div>

                        </div>

                        @if($user_withdrawal_details->cancel_btn_status)
                
                            <a href="{{route('admin.user_withdrawals.paynow', ['user_withdrawal_id' => $user_withdrawal_details->user_withdrawal_id])}}" class="btn btn-success">{{tr('paynow')}}</a>

                            <a href="{{route('admin.user_withdrawals.decline', ['user_withdrawal_id' => $user_withdrawal_details->user_withdrawal_id])}}" class="btn btn-danger">{{tr('decline')}}</a>

                        @endif

                        <table class="table table-borderless table-thead-border">
                            <tbody>
                                <tr>
                                    <td class="text-dark font-weight-bold">{{tr('status')}}</td>
                                    <td class="text-right text-uppercase">{{$user_withdrawal_details->status_formatted}}</td>
                                </tr>

                                <tr>
                                    <td class="text-dark font-weight-bold">{{tr('paid_amount')}}</td>
                                    <td class="text-right text-uppercase">{{$user_withdrawal_details->paid_amount_formatted}}</td>
                                </tr>

                                <tr>
                                    <td class="text-dark font-weight-bold">{{tr('created_at')}}</td>
                                    <td class="text-right text-uppercase">{{ common_date($user_withdrawal_details->created_at , Auth::guard('admin')->user()->timezone) }}</td>
                                </tr>

                                <tr>
                                    <td class="text-dark font-weight-bold">{{tr('updated_at')}}</td>
                                    <td class="text-right text-uppercase">{{ common_date($user_withdrawal_details->updated_at , Auth::guard('admin')->user()->timezone) }}</td>
                                </tr>

                            </tbody>
                        </table>
                    
                    </div>
                
                </div>
                
            </div>

    </div>

</div>

@endsection
<div class="col-md-12 mb-2">

    <form class="col-sm-12 col-sm-offset-6 search_box_css" action="{{route('admin.generated_invoices.index')}}" method="GET" role="search">

        <div class="row input-group">
            <div class="col-md-3"></div>
            <div class="col-md-4">
                <input type="text" class="form-control" name="search_key" placeholder="{{tr('invoice_search_placeholder')}}" value="{{Request::get('search_key')??''}}"> <span class="input-group-btn"></span>
            </div>

            <div class="col-md-2">

                <select class="form-control" name="status">

                    <option value="">{{tr('select_status')}}</option>
                    <option value="{{INVOICE_DRAFT}}" @if(Request::get('status')==INVOICE_DRAFT && Request::get('status')!='' ) selected @endif>{{tr('draft')}}</option>
                    <option value="{{INVOICE_SCHEDULED}}" @if(Request::get('status')==INVOICE_SCHEDULED) selected @endif>{{tr('scheduled')}}</option>
                    <option value="{{INVOICE_SENT}}" @if(Request::get('status')==INVOICE_SENT) selected @endif>{{tr('sent')}}</option>
                    <option value="{{INVOICE_PAID}}" @if(Request::get('status')==INVOICE_PAID) selected @endif>{{tr('paid')}}</option>

                </select>
            </div>

            @if(Request::get('pending_invoices'))
            <input type="hidden" name="pending_invoices" value="{{YES}}">
            @endif

            <div class="col-md-2 flex">
                <button type="submit" class="btn btn-info">
                    <span class="glyphicon glyphicon-search"> {{tr('search')}}</span>
                </button> &nbsp;&nbsp;
                <a class="btn btn-danger" href="{{route('admin.generated_invoices.index')}}">{{tr('clear')}}</a>
            </div>
        </div>

    </form>
</div>
@extends('layouts.admin')

@section('page_header',tr('user_disputes'))

@section('breadcrumbs')


<li class="breadcrumb-item active"><a href="javascript:void(0)"></a>{{tr('disputes')}}</li>

@endsection

@section('content')


<div class="card">

    <div class="card-header bg-info">

        <h4 class="m-b-0 text-white">{{ tr('disputes')}}

            <button type="button" class="badge badge-square badge-outline-light" data-toggle="popover" data-content="{{tr('user_disputes_note')}}">?</button>

        </h4>

    </div>

    <div class="card-body">

        <div class="table-responsive">
           
           @include('admin.user-disputes._search')

            <table id="dataTable" class="table data-table">

                <thead>
                    <tr>
                        <th>{{tr('s_no')}}</th>
                        <th>{{tr('name')}}</th>
                        <th>{{tr('dispute_user')}}</th>
                        <th>{{tr('amount')}}</th>
                        <th>{{tr('status')}}</th>
                        <th>{{tr('action')}}</th>
                    </tr>
                </thead>

                <tbody>

                    @foreach($user_disputes as $i => $dispute_details)

                    <tr>
                        <td>{{$i+$user_disputes->firstItem()}}</td>

                        <td>
                            <a href="{{ route('admin.users.view', ['user_id' => $dispute_details->user_id]) }}">{{$dispute_details->DisputeSender->name ?? tr('not_available')}}
                            </a>
                        </td>

                        <td>
                            <a href="{{ route('admin.users.view', ['user_id' => $dispute_details->receiver_user_id]) }}">{{$dispute_details->DisputeReceiver->name ?? tr('not_available')}}
                            </a>
                        </td>

                        <td>{{formatted_amount($dispute_details->amount??'0.00')}}</td>

                        <td>
                            <span class="text-info">{{$dispute_details->status_formatted}}</span>
                        </td>


                        <td>
                            <a class="btn btn-outline-warning btn-sm" href="{{ route('admin.user_disputes.view', ['user_dispute_id' => $dispute_details->id]) }}">
                                {{ tr('view') }}
                            </a>
                        </td>

                    </tr>
                    @endforeach

                </tbody>

            </table>

            <div class="pull-right">{{ $user_disputes->appends(request()->input())->links() }}</div>
        </div>

    </div>

</div>

@endsection
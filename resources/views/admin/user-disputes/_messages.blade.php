@foreach($messages as $value)

@if(in_array($value->message_type, [DISPUTE_SENDER_TO_RECEIVER, DISPUTE_RECEIVER_TO_SENDER]))

 <!-- Media Chat Left -->
<div class="media media-chat">
    <img src="{{$value->Sender->picture}}" class="chat-img rounded-circle" alt="Avata Image" />
    <div class="media-body">
        <div class="text-content">
            <span class="message">{{$value->message}}</span>
            <time class="time">{{$value->created_at->diffForHumans()}}</time>
        </div>
    </div>
</div>

@else

<!-- Media Chat Right -->
<div class="media media-chat media-chat-right">
    <div class="media-body">
        <div class="text-content">
            <span class="message">{{$value->message}}</span>
            <time class="time">{{$value->created_at->diffForHumans()}}</time>
        </div>
    </div>
</div>

@endif

@endforeach
<?php

return [

    // Authentication
    1000 => 'Your account is waiting for admin approval',

    1001 => 'Please verify your email address!!',

    1002 => 'You are not a registered user!!',

    1003 => 'Your session Expired',

    1004 => 'Invalid session. Login again',

    1005 => 'Authentication parameters are invalid.!',

    1006 => 'The record not exists.',

    1007 => 'Your account is waiting for admin approval.',

    1008 => 'The KYC approval is pending. Please upload your documents',

    101 => 'Invalid Input',

    102 => 'Sorry, the username or password you entered do not match.',

    103 => 'Oops! something went wrong. We couldn’t save your changes.',

    104 => 'Invalid email address',

    105 => 'The mail send process is failed!!!',

    106 => 'The mail configuration failed!!!',

    107 => 'The payment configuration is failed',

    108 => 'Sorry, the password is not matched.',

    109 => 'Update the payment mode in account and try again!!!',

    111 => 'Add card and try again.',

    113 => 'Payment failed!!',

    114 => 'Failed to add card! Try again later.',

    115 => 'The forgot password only available for manual login.',

    116 => 'The email verification not yet done Please check you inbox.',

    117 => 'The requested email is disabled by admin.',

    118 => 'The change password only available for manual login.',

    119 => 'The account delete failed',

    120 => 'The card record is not found.',

    121 => 'The payment configuration Failed',

    122 => 'The card update failed.',

    123 => 'Registeration failed!!',

    124 => 'You do not have an account, please register and continue',

    127 => 'You have already logged into this account using another device, please logout and try again',

    128 => 'Details update failed!!',

    129 => 'The invoice is not found',

    130 => 'The user account is not valid',

    131 => 'Your wallet balance is low.',

    132 => 'The withdraw request is not found',

    133 => 'The withdraw cancel is not allowed',

    134 => 'The requested amount is less than the min balance(:other_key)',

    135 => 'The invoice item not found',

    136 => 'The invoice item delete failed',

    137 => 'The dispute record not exits',

    138 => 'The wallet payment record not exits',

    139 => 'The wallet payment is not eligible for dispute',

    140 => 'The dispute raise is failed due to technical error',

    141 => 'Already raised dispute. Please check resolution center',

    142 => 'The dispute cancel is not allowed',

    143 => 'choose whom do you want to send money',

    144 => 'The transaction record not found',

    145 => 'Selected billing account is invalid',

    146 => 'Invalid verification code',

    147 => 'Requested amount should be less than wallet balance',

    148 => 'Please add items to invoice',

];

<?php


Route::group(['middleware' => 'web'], function() {
    
    Route::group(['as' => 'admin.', 'prefix' => 'admin'], function(){

        Route::get('important/constants', 'ApplicationController@list_of_constants');

        Route::get('clear-cache', function() {

            $exitCode = Artisan::call('config:cache');

            return back();

        })->name('clear-cache');

        Route::get('login', 'Auth\AdminLoginController@showLoginForm')->name('login');

        Route::post('login', 'Auth\AdminLoginController@login')->name('login.post');

        Route::get('logout', 'Auth\AdminLoginController@logout')->name('logout');

        /***
         *
         * Admin Account releated routes
         *
         */

        Route::get('profile', 'AdminController@profile')->name('profile');

        Route::post('profile/save', 'AdminController@profile_save')->name('profile.save');

        Route::post('change/password', 'AdminController@change_password')->name('change.password');

        Route::get('/', 'AdminController@index')->name('dashboard');
        
        Route::get('users', 'AdminController@users_index')->name('users.index');

        Route::get('users/create', 'AdminController@users_create')->name('users.create');

        Route::get('users/edit', 'AdminController@users_edit')->name('users.edit');

        Route::post('users/save', 'AdminController@users_save')->name('users.save');

        Route::get('users/view', 'AdminController@users_view')->name('users.view');

        Route::get('users/delete', 'AdminController@users_delete')->name('users.delete');

        Route::get('users/status', 'AdminController@users_status')->name('users.status');

        Route::get('users/verify', 'AdminController@users_verify_status')->name('users.verify');

        Route::get('unverifed_users','AdminController@unverified_users_index')->name('unverified_users.index');

        Route::get('users/document_verify_status', 'AdminController@document_verify_status')->name('users.document_verify_status');

        // settings

        Route::get('settings-control', 'AdminController@settings_control')->name('control');

        Route::get('settings', 'AdminController@settings')->name('settings'); 

        Route::post('settings/save', 'AdminController@settings_save')->name('settings.save'); 

        Route::get('razorpay', 'AdminController@razorpay_settings_control')->name('razorpay_settings_control');

        Route::post('env_settings','AdminController@env_settings_save')->name('env-settings.save');

        // STATIC PAGES

        Route::get('static_pages' , 'AdminController@static_pages_index')->name('static_pages.index');

        Route::get('static_pages/create', 'AdminController@static_pages_create')->name('static_pages.create');

        Route::get('static_pages/edit', 'AdminController@static_pages_edit')->name('static_pages.edit');

        Route::post('static_pages/save', 'AdminController@static_pages_save')->name('static_pages.save');

        Route::get('static_pages/delete', 'AdminController@static_pages_delete')->name('static_pages.delete');

        Route::get('static_pages/view', 'AdminController@static_pages_view')->name('static_pages.view');

        Route::get('static_pages/status', 'AdminController@static_pages_status_change')->name('static_pages.status');

        // Documents CRUD operations

        Route::get('/kyc_documents/index', 'AdminController@kyc_documents_index')->name('kyc_documents.index');

        Route::get('/kyc_documents/create', 'AdminController@kyc_documents_create')->name('kyc_documents.create');

        Route::get('/kyc_documents/edit', 'AdminController@kyc_documents_edit')->name('kyc_documents.edit');

        Route::post('/kyc_documents/save', 'AdminController@kyc_documents_save')->name('kyc_documents.save');

        Route::get('/kyc_documents/view', 'AdminController@kyc_documents_view')->name('kyc_documents.view');

        Route::get('/kyc_documents/delete', 'AdminController@kyc_documents_delete')->name('kyc_documents.delete');

        Route::get('/kyc_documents/status', 'AdminController@kyc_documents_status')->name('kyc_documents.status');


        Route::get('wallets','AdminController@user_wallets_index')->name('user_wallets.index');

        Route::get('wallets/view','AdminController@user_wallets_view')->name('user_wallets.view');

        Route::get('wallets/payments','AdminController@user_wallet_payments')->name('user_wallet_payments.index');

        Route::get('wallets/payments/view','AdminController@user_wallet_payments_view')->name('user_wallet_payments.view');

        Route::get('wallets/payments/status','AdminController@user_wallet_payments_status')->name('user_wallet_payments.status');

        
        Route::get('user/documents/view/','AdminController@user_kyc_documents_view')->name('user_kyc_documents.view');

        Route::get('user/documents/view/','AdminController@user_kyc_documents_view')->name('user_kyc_documents.view');

        Route::get('generated_invoices','AdminController@generated_invoices_index')->name('generated_invoices.index');

        Route::get('generated_invoices/view', 'AdminController@generated_invoices_view')->name('generated_invoices.view');

        Route::get('generated_invoices/delete', 'AdminController@generated_invoices_delete')->name('generated_invoices.delete');

        Route::get('billing_accounts', 'AdminController@user_billing_accounts_index')->name('user_billing_accounts.index');

        Route::get('billing_accounts/create', 'AdminController@user_billing_accounts_create')->name('user_billing_accounts.create');


        Route::post('billing_accounts/save', 'AdminController@user_billing_accounts_save')->name('user_billing_accounts.save');

        Route::get('billing_accounts/view', 'AdminController@user_billing_accounts_view')->name('user_billing_accounts.view');

        Route::get('billing_accounts/edit', 'AdminController@user_billing_accounts_edit')->name('user_billing_accounts.edit');

        Route::get('billing_accounts/delete', 'AdminController@user_billing_accounts_delete')->name('user_billing_accounts.delete');

        Route::get('billing_accounts/status', 'AdminController@user_billing_accounts_status')->name('user_billing_accounts.status');

        Route::get('withdraw-requests','AdminController@user_withdrawals_index')->name('user_withdrawals.index');

        Route::get('withdraw-requests/view','AdminController@user_withdrawals_view')->name('user_withdrawals.view');

        Route::get('withdraw-requests/paynow', 'AdminController@user_withdrawals_paynow')->name('user_withdrawals.paynow');

        Route::get('withdraw-requests/decline', 'AdminController@user_withdrawals_decline')->name('user_withdrawals.decline');
        
        Route::get('resolution-center', 'AdminController@resolution_center_index')->name('resolution_center_index');

        Route::get('user_disputes', 'AdminController@user_disputes_index')->name('user_disputes.index');

        Route::get('user_disputes/view', 'AdminController@user_disputes_view')->name('user_disputes.view');

        Route::get('user_disputes/approve', 'AdminController@user_disputes_approve')->name('user_disputes.approve');

        Route::get('user_disputes/reject', 'AdminController@user_disputes_reject')->name('user_disputes.reject');

        Route::post('user_disputes/view', 'AdminController@user_disputes_message_save')->name('user_disputes.message_save');

        Route::post('inboxes/view', 'AdminController@inboxes_view')->name('inboxes.view');

       
        Route::post('/users/bulk_action', 'AdminController@users_bulk_action')->name('users.bulk_action');


    });

});
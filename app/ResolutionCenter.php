<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResolutionCenter extends Model
{
   	protected $hidden = ['deleted_at', 'id', 'unique_id'];

	protected $appends = ['resolution_center_id','resolution_center_unique_id', 'message_type_formatted'];

    public function getResolutionCenterIdAttribute() {

        return $this->id;
    }

    public function getResolutionCenterUniqueIdAttribute() {

        return $this->unique_id;
    }

    public function getMessageTypeFormattedAttribute() {

        return message_type_formatted($this->message_type);
    }

    public function Receiver() {
        return $this->belongsTo('App\User','receiver_id');
    }

    public function Sender() {
        return $this->belongsTo('App\User','sender_id');
    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['unique_id'] = "RC-".uniqid();
        });

        static::created(function($model) {

            $model->attributes['unique_id'] = "RC-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });
    }
}

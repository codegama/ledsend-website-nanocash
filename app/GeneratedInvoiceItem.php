<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneratedInvoiceItem extends Model
{
    protected $hidden = ['deleted_at', 'id', 'unique_id'];

	protected $appends = ['generated_invoice_item_id', 'generated_invoice_item_unique_id', 'amount_formatted', 'sub_total_formatted', 'tax_price_formatted', 'total_formatted'];

    public function getGeneratedInvoiceItemIdAttribute() {

        return $this->id;
    }

    public function getGeneratedInvoiceItemUniqueIdAttribute() {

        return $this->unique_id;
    }

    public function getTotalFormattedAttribute() {

        return formatted_amount($this->total);
    }

    public function getSubTotalFormattedAttribute() {

        return formatted_amount($this->sub_total);
    }

    public function getAmountFormattedAttribute() {

        return formatted_amount($this->amount);
    }

    public function getTaxPriceFormattedAttribute() {

        return formatted_amount($this->tax_price);
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCommonResponse($query) {
        return $query;
    }

    public static function boot() {

        parent::boot();

        static::creating(function ($model) {

            $model->attributes['unique_id'] = "GIN-"."-".uniqid();
        });

        static::created(function($model) {

            $model->attributes['unique_id'] = "GIN-"."-".$model->attributes['id']."-".uniqid();

            $model->save();
        
        });
    }
}

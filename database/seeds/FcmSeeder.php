<?php

use Illuminate\Database\Seeder;

class FcmSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
    		[
		        'key' => 'user_fcm_sender_id',
		        'value' => ''
		    ],
		    [
		        'key' => 'user_fcm_server_key',
		        'value' => ''
		    ]
		]);
    }
}

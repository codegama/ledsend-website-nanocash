<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAuthRelatedTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('users')) {

            Schema::create('users', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('unique_id')->default(rand());
                $table->string('name');
                $table->string('first_name')->default('');
                $table->string('middle_name')->default('');
                $table->string('last_name')->default('');
                $table->string('username')->nullable();
                $table->string('email')->unique();
                $table->text('about')->nullable();
                $table->enum('gender',['male','female','others'])->default('male');
                $table->string('picture')->default(asset('placeholder.jpg'));
                $table->string('password');
                $table->string('mobile');
                $table->string('address')->default('');
                $table->integer('user_type')->default(0);
                $table->string('payment_mode')->default(CARD);
                $table->string('token');
                $table->string('token_expiry');
                $table->string('device_token')->nullable();
                $table->enum('device_type', ['web', 'android', 'ios'])->default('web');
                $table->enum('login_by', ['manual','facebook','google', 'instagram', 'apple', 'linkedin'])->default('manual');
                $table->string('social_unique_id')->default('');
                $table->tinyInteger('registration_steps')->default(0);
                $table->integer('push_notification_status')->default(YES);
                $table->integer('email_notification_status')->default(YES);
                $table->integer('user_card_id')->default(0);
                $table->integer('is_verified')->default(0);
                $table->integer('is_kyc_document_approved')->default(0);
                $table->string('verification_code')->default('');
                $table->string('verification_code_expiry')->default('');
                $table->timestamp('email_verified_at')->nullable();
                $table->string('timezone')->default('America/Los_Angeles');
                $table->tinyInteger('status')->default(1);
                $table->rememberToken();
                $table->timestamps();
            
            });
        }

        if(!Schema::hasTable('admins')) {

            Schema::create('admins', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('unique_id');
                $table->string('name');
                $table->string('email')->unique();
                $table->string('password');
                $table->string('about')->default('');
                $table->string('mobile')->default('');
                $table->string('picture')->default(asset('placeholder.jpg'));
                $table->enum('gender', ['male', 'female', 'others'])->default('male');
                $table->string('timezone')->default('America/Los_Angeles');
                $table->tinyInteger('status')->default(1);
                $table->rememberToken();
                $table->timestamps();
            });

        }

        if(!Schema::hasTable('user_kyc_documents')) {

            Schema::create('user_kyc_documents', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('unique_id')->default(rand());
                $table->integer('user_id');
                $table->integer('kyc_document_id');
                $table->string('document_file');
                $table->string('document_file_front')->default('');
                $table->string('document_file_back')->default('');
                $table->tinyInteger('is_verified')->default(0)->comment('0 - pending, 1 - approved, 2 - declined');
                $table->string('uploaded_by')->default('user')->comment('user | admin');
                $table->tinyInteger('status')->default(1);
                $table->timestamps();
            });
            
        }

        if(!Schema::hasTable('user_cards')) {

            Schema::create('user_cards', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('unique_id')->default(uniqid());
                $table->integer('user_id');
                $table->string('card_holder_name')->default("");
                $table->string('card_type');
                $table->string('customer_id');
                $table->string('last_four');
                $table->string('card_token');
                $table->integer('is_default')->default(0);
                $table->tinyInteger('status')->default(1);
                $table->timestamps();
            });
            
        }

        if(!Schema::hasTable('user_notifications')) {

            Schema::create('user_notifications', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('unique_id')->default(rand());
                $table->integer('user_id');
                $table->integer('notification_type')->comment('1 - wallet, 2 - loan, 3 - redeem, 4 - documents');
                $table->text('message');
                $table->tinyInteger('status')->default(1);
                $table->timestamps();
            });
            
        }

        if(!Schema::hasTable('user_billing_accounts')) {

            Schema::create('user_billing_accounts', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('unique_id')->unique();
                $table->integer('user_id');
                $table->string('nickname')->nullable();
                $table->string('bank_name');
                $table->string('account_holder_name');
                $table->string('account_number');
                $table->string('ifsc_code');
                $table->string('swift_code')->nullable();
                $table->tinyInteger('is_default')->default(0);
                $table->tinyInteger('status')->default(1);
                $table->timestamps();
            
            });
            
        }

        if(!Schema::hasTable('users')) {
            
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('admins');
        Schema::dropIfExists('user_kyc_documents');
        Schema::dropIfExists('user_cards');
        Schema::dropIfExists('user_notifications');
        Schema::dropIfExists('user_billing_accounts');
    }
}
